package com.example.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.entity.Course;
import com.example.entity.Student;
import com.example.exception.TestException;

@Service
public class StudentsService {
	public void addStudent(Student student) throws TestException {
		// connect with databases, external resources or other apis that store in a
		// repository
		// studentDao.save(student);
		return;
	}

	public void deleteStudent(Student student) throws TestException {
		// studentDao.delete(student);
		return;
	}

	public List<Student> getCourse(Course course) throws TestException {
		// studentDao.findByCourseName(course.getName());
		List<Student> students = new ArrayList<>();
		List<Student> result = students.stream().sorted(Comparator.comparing(Student::getName))
				.collect(Collectors.toList());
		return result;
	}
}
