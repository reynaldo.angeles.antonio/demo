package com.example.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Course;
import com.example.entity.Student;
import com.example.exception.TestException;
import com.example.service.StudentsService;

@RestController
public class StudentsController {

	@Autowired
	private StudentsService studentsService;
	
	@PostMapping("/students")
	public ResponseEntity<String> addStudent(@RequestParam Student student) {
			try {
			studentsService.addStudent(student);
			return new ResponseEntity<>(HttpStatus.OK);
			}catch(TestException testException) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}
	
	@DeleteMapping("/students")
	public ResponseEntity<String> deleteStudent(@RequestParam Student student) {
		try {
		studentsService.deleteStudent(student);
		return new ResponseEntity<>(HttpStatus.OK);
		}catch(TestException testException) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
	@GetMapping("/students")
	public ResponseEntity<List<Student>> getStudents(@RequestParam Course course){
		try {
			List<Student> students = studentsService.getCourse(course);
			return new ResponseEntity<>(students, HttpStatus.OK);
			}catch(TestException testException) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}
	
	
}
