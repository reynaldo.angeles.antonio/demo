package com.example.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Course;
import com.example.entity.Student;
import com.example.exception.TestException;
import com.example.service.CourseService;

@RestController
public class CourseController {
	@Autowired
	private CourseService courseService;
	
	@PostMapping("/courses")
	public ResponseEntity<String> addCourse(@RequestParam Course course) {
			try {
			courseService.addCourse(course);
			return new ResponseEntity<>(HttpStatus.OK);
			}catch(TestException testException) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}
	
	@DeleteMapping("/courses")
	public ResponseEntity<String> deleteCourse(@RequestParam Course course) {
		try {
		courseService.deleteCourse(course);
		return new ResponseEntity<>(HttpStatus.OK);
		}catch(TestException testException) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
	@PostMapping("/courses/studens")
	public ResponseEntity<String> addStudentCourse(@RequestParam Course course, @RequestParam Student student) {
		try {
			Course courseActual = courseService.findCourse(course);
			if(courseActual != null) {
				courseService.addStudentToCourse(course, student);
			}else {
				return new ResponseEntity<>("The course does not exist",HttpStatus.BAD_REQUEST);
			}

		return new ResponseEntity<>(HttpStatus.OK);
		}catch(TestException testException) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
}
